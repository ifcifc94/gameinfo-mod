package com.ifcifc.gameinfo.Logic.Modules;

import com.ifcifc.gameinfo.Logic.ModuleController.ModuleBase;
import com.ifcifc.gameinfo.Logic.ModuleController.ModuleRegNameAnnotation;
import com.ifcifc.gameinfo.Logic.ModuleController.ModuleRegisterAnnotation;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.world.ClientWorld;

@ModuleRegNameAnnotation(RegName="TickTime")
public class mTickTime implements ModuleBase {

    public static final float DAY = 24000.0F;
    public static final float HOUR = 1000.0F;
    public static final float MINUTE = 16.66F;

    public long day, hour, minute;
    public boolean isEnable = true;

    @Override
    public void update(MinecraftClient tick) {
        if(!isEnable)return;
        final ClientWorld world = tick.world;
        if(world==null)return;

        long WorldTime = world.getTimeOfDay();
        if (WorldTime > DAY) WorldTime = (long) (WorldTime % DAY);

        long time = (long) ((WorldTime + HOUR * 6) % DAY);

        day     = (long) (world.getTimeOfDay() / DAY);
        hour    = (long) (time / HOUR);
        minute  = (long) ((time % HOUR) / MINUTE);
    }

    @Override
    public void setEnable(boolean v) {
        isEnable = v;
    }

    @Override
    public boolean isEnable() {
        return isEnable;
    }
    @ModuleRegisterAnnotation(Funcion = "Day", description = "Get world day")
    public long getDay(){
        return day;
    }

    @ModuleRegisterAnnotation(Funcion = "Hour", description = "Get world hour")
    public long getHour() {
        return hour;
    }

    @ModuleRegisterAnnotation(Funcion = "Minute", description = "Get world minute")
    public long getMinute(){
        return minute;
    }

    @ModuleRegisterAnnotation(Funcion = "FixDay", description = "Get world day, if less than 10, Add a 0 to the front")
    public String getFixDay(){
        return ((day<10)? "0":"") + day;
    }

    @ModuleRegisterAnnotation(Funcion = "FixHour", description = "Get world hour, if less than 10, Add a 0 to the front")
    public String getFixHour() {
        return ((hour<10)? "0":"") + hour;
    }

    @ModuleRegisterAnnotation(Funcion = "FixMinute", description = "Get world minute, if less than 10, Add a 0 to the front")
    public String getFixMinute(){
        return ((minute<10)? "0":"") + minute;
    }


    @Override
    public boolean isHidden(){
        return !isEnable;
    }
}
