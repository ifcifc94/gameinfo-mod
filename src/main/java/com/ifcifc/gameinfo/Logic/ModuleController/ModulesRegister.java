package com.ifcifc.gameinfo.Logic.ModuleController;

import com.ifcifc.gameinfo.Logic.Modules.*;
import net.minecraft.client.MinecraftClient;

import java.util.ArrayList;

public class ModulesRegister {
    public static ArrayList<ModuleBase> ModulesList = new ArrayList<>();

    public static mTickFPSClient    TickFPS         = new mTickFPSClient();
    public static mTickTime         TickTime        = new mTickTime();
    public static mLightPlayer      Light           = new mLightPlayer();
    public static mCoordinates      Coordinates     = new mCoordinates();
    public static mRealTime         RealTime        = new mRealTime();
    public static mPlayer           Player          = new mPlayer();
    public static mTranslate        Translate       = new mTranslate();
    public static mGameInfo         GameInfo        = new mGameInfo();

    public static void initialice(){
        register(TickTime);
        register(TickFPS);
        register(Light);
        register(Coordinates);
        register(RealTime);
        register(Player);
        register(Translate);
        register(GameInfo);
    }

    public static void register(ModuleBase M){
        ModulesList.add(M);
        ModulesController.register(M);
    }

    public static String getDimensionName(){
        return MinecraftClient.getInstance().world.getRegistryKey().getValue().getPath();
    }

}
