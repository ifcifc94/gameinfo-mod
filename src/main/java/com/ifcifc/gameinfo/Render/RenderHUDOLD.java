package com.ifcifc.gameinfo.Render;

public class RenderHUDOLD {
/*
    public static HudStaticText     TEXT_FPS                = new HudStaticText("",0,0);
    public static HudStaticText     TEXT_DATE               = new HudStaticText("",0,0);
    public static HudStaticText     TEXT_POSITION           = new HudStaticText("",0,0);
    public static HudStaticText     TEXT_NETHER_POSITION    = new HudStaticText("",0,0);

    public static boolean           isAlive                 = true;

    private static boolean fpsline=false;

    public static void initialize() {
        HudRenderCallback.EVENT.register(RenderHUDOLD::RenderText);
    }

    public static void onClientLoad() {
        toggleHUD(false);
        if(Config.options.isDisableAutoJump && MinecraftClient.getInstance().options.autoJump) MinecraftClient.getInstance().options.autoJump = false;
    }

    public static void hiddenHud(boolean isHidden, boolean isChangeHidden, boolean isKey, boolean force){
        hiddenHud(isHidden,Config.options.HUDINFOSHOW,isChangeHidden,isKey,force);
    }

    public static void hiddenHud(boolean isHidden, int HUD, boolean isChangeHidden, boolean isKey, boolean force){
        if((!isKey && !force) && Config.options.isKeyHidden)return;

        if(isKey)Config.options.isKeyHidden=isHidden;
        if(isChangeHidden)Config.options.isHidden = isHidden;

        if (force || getBit(HUD,0)) RenderHUDOLD.TEXT_DATE.setHidden(isHidden);
        if (force || getBit(HUD,1)) RenderHUDOLD.TEXT_POSITION.setHidden(isHidden);
        if (force || getBit(HUD,2)) RenderHUDOLD.TEXT_NETHER_POSITION.setHidden(isHidden);

        if (force || getBit(HUD,3)) RenderHUDOLD.TEXT_FPS.setHidden(isHidden);
    }

    public static void toggleLightHUD(){
        Config.options.HUDLightSHOW++;
        if(Config.options.HUDLightSHOW>3)Config.options.HUDLightSHOW=0;
        Config.options.isBlockLightShow=getBit(Config.options.HUDLightSHOW,0);
        Config.options.isSunLightShow=getBit(Config.options.HUDLightSHOW,1);

    }

    private static void checkNewLine(){
        if(fpsline == Config.options.fpsDetailShow)return;
        fpsline = Config.options.fpsDetailShow;
        if(fpsline) {
            if(Config.options.HUDLIMIT != 15)Config.options.HUDINFOSHOW += 8;
        }else if(Config.options.HUDINFOSHOW>7){
            Config.options.HUDINFOSHOW -= 8;
        }

        Config.options.HUDLIMIT = (fpsline)? 15:7;
    }

    public static void toggleHUD(){
        toggleHUD(true);
    }
    public static void toggleHUD(boolean isChangeHUD){
        checkNewLine();
        hiddenHud(true,7,true,false,true);
        if(isChangeHUD)Config.options.HUDINFOSHOW++;
        //System.out.println((Config.options.HUDINFOSHOW>Config.options.HUDLIMIT)+ " - " + Config.options.HUDLIMIT + " - " + Config.options.HUDINFOSHOW);
        if(Config.options.HUDINFOSHOW>Config.options.HUDLIMIT)Config.options.HUDINFOSHOW=1;

        int mov;
        int y=4;
        try {
            mov = MinecraftClient.getInstance().textRenderer.fontHeight;
        } catch (Exception e) {
            mov = 8;
            e.printStackTrace();
        }
        if (getBit(Config.options.HUDINFOSHOW,3)) {
            TEXT_FPS.setPosition(4, y);
            y += mov;//TEXT_DATE.getHeight();
        }

        if (getBit(Config.options.HUDINFOSHOW,0)) {
            TEXT_DATE.setPosition(4, y);
            y+=mov;//TEXT_DATE.getHeight();
        }
        if (getBit(Config.options.HUDINFOSHOW,1)) {
            TEXT_POSITION.setPosition(4, y);
            y+=mov;//TEXT_POSITION.getHeight();
        }
        if (getBit(Config.options.HUDINFOSHOW,2)) {
            TEXT_NETHER_POSITION.setPosition(4, y);
        }
        hiddenHud(false,true,false,false);
    }


    public static boolean getBit(int data, int position)
    {
        return (byte) ((data >> position) & 1) == 1;
    }

    public static void RenderText(MatrixStack matrixStack, float v) {
        TextRenderer textRenderer = MinecraftClient.getInstance().textRenderer;

        RenderHUDOLD.TEXT_DATE.draw(matrixStack, textRenderer);
        RenderHUDOLD.TEXT_POSITION.draw(matrixStack, textRenderer);
        RenderHUDOLD.TEXT_NETHER_POSITION.draw(matrixStack, textRenderer);
        RenderHUDOLD.TEXT_FPS.draw(matrixStack, textRenderer);

    }*/
}
