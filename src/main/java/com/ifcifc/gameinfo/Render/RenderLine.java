package com.ifcifc.gameinfo.Render;

import com.ifcifc.gameinfo.Config.Config;
import com.ifcifc.gameinfo.Logic.UpdateHUD;
import com.ifcifc.gameinfo.Util.UI.HudStaticText;
import net.minecraft.client.MinecraftClient;

public class RenderLine extends HudStaticText{
    public int pos, bit;
    public float width;
    public String fromData;

    public RenderLine(int pos, int bit) {
        super("",0,0);
        this.pos    = pos;
        this.bit    = bit;
        this.width  = 0;
    }

    public RenderLine(int pos, int bit, String fromData) {
        super("",0,0);
        this.pos = pos;
        this.bit = bit;
        this.fromData = fromData;
        this.width  = 0;
    }

    @Override
    public void setText(String text){
        try {
            super.setText(text);
            this.width = getLineWith();
        }catch (Exception e){
            if(MinecraftClient.getInstance()==null || MinecraftClient.getInstance().textRenderer==null)return;

            e.printStackTrace();
        }
    }

    public  float getLineWith() {

        String T = getText().replaceAll("§[0-9a-z]", "");

        return (float) MinecraftClient.getInstance().textRenderer.getWidth(T);
    }

    public float getWidth() {
        float ret = width;

        if(Config.options.useScale)
            ret *= Config.options.getScaleX();

        if (!this.isUseDefaultScale())
            ret *= this.getScaleX();

        return ret;
    }

    public float getHeight(){
        if (this.isUseDefaultScale()) return 9;
        return 9 * getScaleY() * 0.9f;
    }

    public void update(MinecraftClient tick){
        if(this.isHidden())return;
        try{
            this.setText((String) UpdateHUD.result.get(fromData));
        }catch (Exception e){
            //e.printStackTrace();
        }
    }
}
