package com.ifcifc.gameinfo.Util;

import com.ifcifc.gameinfo.Config.Config;
import com.ifcifc.gameinfo.Logic.ModuleController.ModulesController;
import com.ifcifc.gameinfo.Logic.UpdateHUD;
import com.ifcifc.gameinfo.MainMod;
import com.ifcifc.gameinfo.Render.RenderHUD;
import com.ifcifc.gameinfo.Render.RenderLine;
import com.ifcifc.gameinfo.Util.LineBuilder.Utils.FunctionData;
import com.ifcifc.gameinfo.Util.LineBuilder.Utils.LogicData;

import javax.script.ScriptException;
import java.io.*;
import java.util.Map;

public class PackageDebug {
    public static int index = 0;

    public static void initialize(){

    }

    public static void changePackage(boolean up){
        if(!Config.options.DebugPackages)return;
        if(up)
            index++;
        else
            index--;

        if(index<0)index = ModulesController.ModulesList.size()-1;
        if(index>= ModulesController.ModulesList.size())index=0;

        final LogicData logicData = ModulesController.ModulesList.get(index);
        if(logicData.getPackage().equals("Translate")){
            changePackage(up);
            return;
        }
        
        MainMod.sendMessage("§ePackage: §r" + logicData.getPackage(), true);
        UpdateHUD.reload();
    }

    public static void update(){
        String Code =   "\nvar ModulesRegister =  Java.type('com.ifcifc.gameinfo.Logic.ModuleController.ModulesRegister');\nvar ret={};\n\n";
        Code +="function Update(){\n";
        int inx=0;

        LogicData M = ModulesController.ModulesList.get(index);

        for (Map.Entry<String, FunctionData> F : M.getFunctions().entrySet()) {
            FunctionData Fc = F.getValue();
            String Line = "Line" + inx;
            String Argument = "";

            if(Fc.acceptArguments){
                Argument = Fc.defaultArguments;
            }
///////////////////////////////////////
            RenderHUD.Lines.add(new RenderLine(inx,1, Line));
///////////////////////////////////////
            Code += "    ret['" + Line + "'] = '§a" + Fc.description.replace("'", "\"") + "§r - §e" + Fc.Function + ": §r';\n";
            Code += "    if(!ModulesRegister."+Fc.getHiddenFuncion()+"()){\n";
            Code += "       ret['" + Line + "'] += ModulesRegister."+ Fc.getFunction() + ((Argument.isEmpty())? "()" : ("(" + Argument +")")) + ";\n";
            Code += "    }else{\n";
            Code += "       ret['" + Line + "'] += 'Null'\n";
            Code += "    }\n";
            inx++;
        }


        Code += "    return ret;\n";
        Code += "}\n";
//////////////////////////////////////////////
        //System.out.println(Code);
        /*try {
            FileWriter sFile = new FileWriter(new File( "Debug.js"));
            sFile.write(Code);
            sFile.close();
        } catch (IOException e) {
            e.printStackTrace();
        }*/
/////////////////////////////////////////////
        Config.options.HUDLIMIT = (int)Math.pow(2, RenderHUD.Lines.size())-1;

        Config.options.HUDINFOSHOW = Config.options.HUDLIMIT;

        RenderHUD.hiddenHud(true, true, true);
        RenderHUD.sortLines();
        RenderHUD.toggleHUD();
        RenderHUD.hiddenHud(false, true, false);

        try {
            UpdateHUD.Engine.eval(Code);
            //Lines.saveJS(CODE,"Line");
        } catch (ScriptException e) {
            e.printStackTrace();

            try {
                FileWriter sFile = new FileWriter(new File( "Debug.js"));
                sFile.write(Code);
                sFile.close();

                StringWriter sw = new StringWriter();
                e.printStackTrace(new PrintWriter(sw));

                sFile = new FileWriter(new File( "Debug.log"));
                sFile.write(sw.toString());
                sFile.close();
            } catch (IOException ee) {
                ee.printStackTrace();
            }
            System.exit(1);
        }
    }
}
