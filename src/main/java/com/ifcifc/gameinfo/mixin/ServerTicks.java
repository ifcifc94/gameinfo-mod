package com.ifcifc.gameinfo.mixin;

import net.minecraft.server.MinecraftServer;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.gen.Accessor;

@Mixin(MinecraftServer.class)
public interface ServerTicks {
    @Accessor("tickTime")
    public abstract float getTickTime();
}
