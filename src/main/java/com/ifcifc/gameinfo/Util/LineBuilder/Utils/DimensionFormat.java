package com.ifcifc.gameinfo.Util.LineBuilder.Utils;


public class DimensionFormat {
    public final String         DimensionID;
    public final LineFormat[]   Format;

    public DimensionFormat(LineFormat[] format, String dimensionID) {
        DimensionID = dimensionID;
        Format = format;
    }
}
