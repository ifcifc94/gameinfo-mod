package com.ifcifc.gameinfo.Util.LineBuilder.Utils;

public class FunctionData {
    public final String     Function,           HiddenFuncion, Package, defaultArguments, description, Arguments;
    public final boolean    acceptArguments,    isNeededArguments;

    public FunctionData(String function, String hiddenFuncion, String aPackage, String defaultArguments, String description, String arguments, boolean acceptArguments, boolean isNeededArguments) {
        Function = function;
        HiddenFuncion = hiddenFuncion;
        Package = aPackage;
        this.defaultArguments = defaultArguments;
        this.description = description;
        Arguments = arguments;
        this.acceptArguments = acceptArguments;
        this.isNeededArguments = isNeededArguments;
    }

    public String getFunction(){
        return Package + "." + Function;
    }
    public String getHiddenFuncion(){
        return Package + "." + HiddenFuncion;
    }


}
