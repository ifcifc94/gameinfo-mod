package com.ifcifc.gameinfo.Logic;

import com.ifcifc.gameinfo.Config.Config;
import com.ifcifc.gameinfo.Config.Options;
import com.ifcifc.gameinfo.Lines;
import com.ifcifc.gameinfo.Render.RenderHUD;
import com.ifcifc.gameinfo.Render.RenderLine;
import com.ifcifc.gameinfo.Util.LineBuilder.LineConstructor;
import com.ifcifc.gameinfo.Util.LineBuilder.Utils.LineClass;
import com.ifcifc.gameinfo.Util.LineBuilder.Utils.LinePackageClass;

import javax.script.ScriptException;
import java.io.File;
import java.io.FileReader;

public class LoadHUD {
    public static LinePackageClass  defaultHUD=null;
    public static String            CODE="";

    public static void initialize(){
        File file = new File(Config.HUDPath, Config.options.HUDFILE+".json");

        if(!file.exists()){
            file.getParentFile().mkdirs();

            Config.options.HUDFILE = new Options().HUDFILE;
            Config.updateSave();
            file = new File(Config.HUDPath, Config.options.HUDFILE+".json");

            Lines.makePackage(file);
        }

        boolean ERROR=true;
        int E=1;
        while (ERROR && E>0){
            try {
                defaultHUD = Config.gson.fromJson(new FileReader(file), LinePackageClass.class);
                ERROR = false;
            } catch (Exception e) {
                e.printStackTrace();
                E--;
                file = new File(Config.HUDPath, Config.options.HUDFILE+".json");
                if(!file.exists()){
                    Lines.makePackage(file);
                }
            }
        }

        build();

        Config.options.HUDLIMIT = (int)Math.pow(2, RenderHUD.Lines.size())-1;

        Config.options.HUDINFOSHOW = Config.options.HUDLIMIT;

        RenderHUD.sortLines();

        try {
            UpdateHUD.Engine.eval(CODE);
            //Lines.saveJS(CODE,"Line");
        } catch (ScriptException e) {
            e.printStackTrace();
            System.exit(1);
        }
    }

    private static void build(){
        LineConstructor LC = new LineConstructor();
        String Code =   "var ModulesRegister =  Java.type('com.ifcifc.gameinfo.Logic.ModuleController.ModulesRegister');\nvar ret={};\n\n";
        int bit = 0;
        RenderHUD.Lines.clear();
        for(LineClass L : defaultHUD.Lines){
            Code += LC.buildLine(L) + "\n";

            RenderLine RL  = new RenderLine(L.Index,bit, L.Name);
            RL.setScaleX(L.ScaleX);
            RL.setScaleY(L.ScaleY);
            RL.setUseDefaultScale(L.useDefaultScale);
            RenderHUD.Lines.add(RL);
            bit++;
        }

        Config.options.HUDLIMIT = (int)(Math.pow(2,bit)-1);

        if(Config.options.HUDINFOSHOW>Config.options.HUDLIMIT)
            Config.options.HUDINFOSHOW=Config.options.HUDLIMIT;

        Config.updateSave();

        Code+="function Update(){\n";
        for(LineClass L : defaultHUD.Lines){
            Code+="    "+ L.Name +"();\n";
        }
        Code +="    return ret;\n}\n";

        CODE = Code;
    }
}
