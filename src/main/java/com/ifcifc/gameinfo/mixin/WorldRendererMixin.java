package com.ifcifc.gameinfo.mixin;

import net.minecraft.block.entity.BlockEntity;
import net.minecraft.client.render.WorldRenderer;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.gen.Accessor;

import java.util.Set;

@Mixin(WorldRenderer.class)
public interface WorldRendererMixin {
    @Accessor("regularEntityCount")
    abstract int getRegularEntityCount();

    @Accessor("noCullingBlockEntities")
    abstract Set<BlockEntity> getnoCullingBlockEntities();
}

