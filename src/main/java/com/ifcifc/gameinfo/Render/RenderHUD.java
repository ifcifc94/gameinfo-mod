package com.ifcifc.gameinfo.Render;

import com.ifcifc.gameinfo.Config.Config;
import com.ifcifc.gameinfo.Logic.UpdateHUD;
import com.ifcifc.gameinfo.Util.Other;
import net.fabricmc.fabric.api.client.rendering.v1.HudRenderCallback;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.font.TextRenderer;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.util.Language;

import java.util.ArrayList;

public class RenderHUD {

    public static ArrayList<RenderLine> Lines = new ArrayList<>();

    private static Language language = null;

    //public static float Add=0;

    public static void initialize() {
        HudRenderCallback.EVENT.register(RenderHUD::RenderText);
        language = Language.getInstance();

        RenderHUD.sortLines();
    }

    public static void sortLines(){
        if(Config.options.sortByWidth){
            if(Config.options.sortInvest){
                Lines.sort((a, b) -> (a.getWidth() >= b.getWidth()) ? -1 : 1);
            }else{
                Lines.sort((a, b) -> (a.getWidth() <= b.getWidth()) ? -1 : 1);
            }

        }else{

            if(Config.options.sortInvest){
                Lines.sort((a, b) -> (a.pos >= b.pos) ? -1 : 1);
            }else{
                Lines.sort((a, b) -> (a.pos <= b.pos) ? -1 : 1);
            }
        }
    }

    public static void onClientLoad() {
        if(Config.options.DisableAutoJump && MinecraftClient.getInstance().options.autoJump) MinecraftClient.getInstance().options.autoJump = false;
        toggleHUD();
    }

    public static void toggleHiddenHud(boolean isKey, boolean force){
        hiddenHud(!Config.options.Hidden, isKey, force);
    }

    public static void hiddenHud(boolean isHidden,  boolean isKey, boolean force){
        if(isKey)Config.options.Hidden=isHidden;
        if(!(isKey || force) && Config.options.Hidden)return;

        RenderHUD.Lines.forEach(L->{
            if (force || Other.getBit(Config.options.HUDINFOSHOW,L.bit)){
                L.setHidden(isHidden);
            }
        });

    }

    public static float getMaxLineWith() {
        float x = 0;
        for (RenderLine L : Lines) {
            if (L.getText().trim().isEmpty() || L.isHidden()) continue;
            if (!Other.getBit(Config.options.HUDLIMIT, L.bit)) continue;

            float tX =L.getWidth();
            if (tX > x) x = tX;
        }
        return x;
    }
    public static float getMaxLineHeight() {
        float y=4;
        try {

            for (RenderLine L : Lines) {
                if (L.getText().trim().isEmpty() || L.isHidden()) continue;
                if (!Other.getBit(Config.options.HUDLIMIT, L.bit)) continue;

                y += L.getHeight();
            }

        } catch (Exception e) {
        }
        return y;
    }


    public static void toggleHUD(){
        //final float mov= 9;//MinecraftClient.getInstance().textRenderer.fontHeight;
        float y=4;
        try {
            if(Config.options.sortByWidth)sortLines();

            float width = MinecraftClient.getInstance().getWindow().getScaledWidth();
            float ScaleX = Config.options.getScaleX();
            float ScaleY = Config.options.getScaleY();

            if(Config.options.DownHUD)
                y=(MinecraftClient.getInstance().getWindow().getScaledHeight() -
                  ((Config.options.useScale)? (getMaxLineHeight()*ScaleY) : getMaxLineHeight()));

            for (RenderLine L : Lines) {
                if (L.getText().trim().isEmpty() || L.isHidden()) continue;
                if (!Other.getBit(Config.options.HUDLIMIT, L.bit)) continue;

                float x = (Config.options.RightHUD)? (width - L.getWidth() - 3):2;

                if(Config.options.useScale){
                    L.setPosition(x/ScaleX, y/ScaleY);
                    y += L.getHeight()*ScaleY;
                }else{
                    L.setPosition(x, y);
                    y += L.getHeight();
                }

            }



        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void rotateHUD(){
        int max = RenderHUD.Lines.size();
        Lines.forEach(L->{
            L.pos++;
            if(L.pos>=max)L.pos=0;
        });
        RenderHUD.sortLines();
    }


    public static void RenderText(MatrixStack matrixStack, float v) {
        if(UpdateHUD.isPause || UpdateHUD.checkIfHidden())return;

        TextRenderer textRenderer = MinecraftClient.getInstance().textRenderer;

        matrixStack.push();
        if(Config.options.useScale)
            matrixStack.scale(Config.options.getScaleX(), Config.options.getScaleY(), 1);

        try{
            RenderHUD.Lines.forEach(L-> L.draw(matrixStack, textRenderer));
        }catch (Exception E){

        }



        matrixStack.pop();
    }
}
