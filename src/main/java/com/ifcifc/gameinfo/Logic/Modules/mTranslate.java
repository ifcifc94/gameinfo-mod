package com.ifcifc.gameinfo.Logic.Modules;

import com.ifcifc.gameinfo.Logic.ModuleController.ModuleBase;
import com.ifcifc.gameinfo.Logic.ModuleController.ModuleRegNameAnnotation;
import com.ifcifc.gameinfo.Logic.ModuleController.ModuleRegisterAnnotation;
import net.minecraft.client.MinecraftClient;
import net.minecraft.util.Language;


@ModuleRegNameAnnotation(RegName="Translate")
public class mTranslate implements ModuleBase{
    public boolean isEnable = true;

    public mTranslate() {

    }

    @Override
    public void update(MinecraftClient tick) {

    }

    @Override
    public boolean isHidden() {
        return !isEnable;
    }

    @Override
    public void setEnable(boolean v) {
        isEnable = v;
    }

    @Override
    public boolean isEnable() {
        return isEnable;
    }

    @ModuleRegisterAnnotation(Funcion = "TranslateText", isNeededArguments = true, acceptArguments=true, description = "Get translate text", Arguments = "[Key:string]", defaultArguments = "'text.gameinfo.test'")
    public String getTranslateText(String key){

        final Language instance = Language.getInstance();

        if(instance!=null){

            return instance.get(key);
        }

        return "null";
    }
}
