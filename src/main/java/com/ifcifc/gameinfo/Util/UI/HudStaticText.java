package com.ifcifc.gameinfo.Util.UI;

import net.minecraft.client.font.TextRenderer;
import net.minecraft.client.util.math.MatrixStack;

public class HudStaticText {
    private String text;
    private String shadowText;
    private int color;
    private int shadowColor;
    private float x;
    private float y;
    private float shadowOffset;
    private float XOffset;
    private float YOffset;
    private float ScaleX, ScaleY;
    private boolean useDefaultScale;
    private boolean isHidden;

    public HudStaticText(String text, int x, int y){
        this(text, 0xFFFFFFFF,0x00000000, x, y,1);
    }

    public HudStaticText(String text, int color, int shadowColor, int x, int y, int shadowOffset) {
        this.setText(text);
        this.color = color;
        this.shadowColor = shadowColor;
        this.x = x;
        this.y = y;
        this.isHidden=false;
        this.setShadowOffset(shadowOffset);

        this.ScaleX=1;
        this.ScaleY=1;
        this.useDefaultScale=true;
    }

    public void setText(String text) {
        this.text = text;
        this.shadowText = text.replaceAll("§[0-9a-z]", "");
    }

    public String getText(){
        return this.text;
    }

    public void setColor(int Color){
        this.color=Color;
    }
    public void setShadowColor(int Color){
        this.shadowColor=Color;
    }

    public int getColor(){
        return this.color;
    }

    public int getShadowColor(){
        return this.shadowColor;
    }

    public boolean isHidden() {
        return isHidden;
    }

    public void setShadowOffset(float offset){
        this.shadowOffset=offset;
        updateOffSet();
    }

    private void updateOffSet(){
        this.XOffset = this.x + this.shadowOffset;
        this.YOffset = this.y + this.shadowOffset;
    }

    public void setPosition(float x, float y){
        this.setX(x);
        this.setY(y);
    }

    public float getX() {
        return x;
    }
    public void setX(float x) {
        this.x = x;
        updateOffSet();
    }
    public float getY() {
        return y;
    }

    public void setY(float y) {
        this.y = y;
        updateOffSet();
    }

    public void setHidden(boolean hidden) {
        isHidden = hidden;
    }

    public void setScaleX(float scaleX) {
        ScaleX = scaleX;
    }

    public void setScaleY(float scaleY) {
        ScaleY = scaleY;
    }

    public void setUseDefaultScale(boolean useDefaultScale) {
        this.useDefaultScale = useDefaultScale;
    }

    public float getScaleX() {
        return ScaleX;
    }

    public float getScaleY() {
        return ScaleY;
    }

    public boolean isUseDefaultScale() {
        return useDefaultScale;
    }

    public void draw(MatrixStack matrixStack, TextRenderer textRenderer){
        if(this.isHidden())return;

        if(useDefaultScale){
            textRenderer.draw(matrixStack, this.shadowText, this.XOffset, this.YOffset, this.shadowColor);
            textRenderer.draw(matrixStack, this.text,x,y,this.color);
        }else{
            matrixStack.push();

            matrixStack.scale(ScaleX,ScaleY,1);
            float tx=x/ScaleX;
            float ty=y/ScaleY;
            float tXOffset=XOffset/ScaleX;
            float tYOffset=YOffset/ScaleY;
            textRenderer.draw(matrixStack, this.shadowText, tXOffset, tYOffset, this.shadowColor);
            textRenderer.draw(matrixStack, this.text,tx,ty,this.color);

            matrixStack.pop();
        }



    }
}
